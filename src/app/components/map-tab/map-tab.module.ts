import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MapTabPage } from './map-tab.page';
import { WorkerInfoPage } from './worker-info/worker-info.page';

const routes: Routes = [
  {
    path: '',
    component: MapTabPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MapTabPage]
})
export class MapTabPageModule {}
