import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerInfoPage } from './worker-info.page';

describe('WorkerInfoPage', () => {
  let component: WorkerInfoPage;
  let fixture: ComponentFixture<WorkerInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkerInfoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
